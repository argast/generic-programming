import shapeless._

/**
  * Filtering of coproduct instances based on what trait they have as a type param.
  * Input is a mix of all required traits, output is PropertyInfo of all matching coproduct singleton types.
  * Some additional typeclasses are also included to see if further derivation is possible.
  */
object CoproductFiltering extends App {

  trait TypeName[T] {
    val name: String
  }
  implicit val intTypeName: TypeName[Int] = new TypeName[Int] {
    override val name: String = "int"
  }
  implicit val stringTypeName: TypeName[String] = new TypeName[String] {
    override val name: String = "string"
  }
  implicit val longTypeName: TypeName[Long] = new TypeName[Long] {
    override val name: String = "long"
  }

  trait A
  trait B
  trait C

  sealed trait Property[-A, +B] {
    val name: String
  }
  case class PropertyInfo(name: String, `type`: String)
  case object PA extends Property[A, Int] {
    override val name: String = "P_A"
  }
  case object PB extends Property[B, String] {
    override val name: String = "P_B"
  }
  case object PC extends Property[C, Long] {
    override val name: String = "P_C"
  }

  trait PropertyDiscovery[Mix] {
    def properties(): Seq[PropertyInfo]
  }

  object PropertyDiscovery {

    trait PropertyDiscoveryAux[Mix, Repr] {
      def properties(): Seq[PropertyInfo]
    }

    implicit def cnilPropertyDiscovery[Mix, T]: PropertyDiscoveryAux[Mix, CNil] = () => Seq.empty

    implicit def coproductPropertyDiscovery[Mix, A, B, H, T <: Coproduct](
        implicit w: Witness.Aux[H],
        i: H <:< Property[A, B],
        tn: TypeName[B],
        td: Lazy[PropertyDiscoveryAux[Mix, T]]
    ): PropertyDiscoveryAux[Mix, H :+: T] = () => {
      PropertyInfo(w.value.name, tn.name) +: td.value.properties
    }

    implicit def genericPropertyDiscovery[Mix, Repr <: Coproduct](
        implicit
        gen: Generic.Aux[Property[Mix, _], Repr],
        sg: Lazy[PropertyDiscoveryAux[Mix, Repr]]
    ): PropertyDiscovery[Mix] = () => sg.value.properties()
  }

  import PropertyDiscovery._
  println(implicitly[PropertyDiscovery[A with C]].properties())

}
