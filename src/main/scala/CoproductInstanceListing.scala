import shapeless._
import shapeless.ops.coproduct.Reify
import shapeless.ops.hlist.ToTraversable

/**
  * Turn coproduct of singleton types into a list of instances
  */
object CoproductInstanceListing extends App {

  sealed trait Property {
    val name: String
  }
  case object PA extends Property {
    override val name: String = "P_A"
  }
  case object PB extends Property {
    override val name: String = "P_B"
  }
  case object PC extends Property {
    override val name: String = "P_C"
  }

  val g = Generic[Property]
  val reify = Reify[g.Repr]
  val toList = ToTraversable[reify.Out, List]
  val instances: List[Property] = toList(reify())
  println(instances)
  println(instances.map(_.name))
}
