import io.circe._
import shapeless.Witness.Aux
import shapeless._

import scala.language.experimental.macros

sealed trait Definition {
  val id: String
  type Value
}

case object Name extends Definition {
  val id = "name"
  type Value = String
}

case object Age extends Definition {
  val id = "age"
  type Value = Int
}

case class Attribute[D <: Definition](value: D#Value)(
    implicit w: Witness.Aux[D]
) {
  lazy val definition: D = w.value
}

trait AttributeInstances[D <: Definition] {
  def decoder: Decoder[Attribute[D]]
}

object Attribute {

  implicit class DefinitionOps[D <: Definition: Witness.Aux](d: D) {
    def apply(v: D#Value): Attribute[D] = Attribute(v)
  }

  trait AttributeInstancesAux[D <: Definition, Repr] {
    def decoder: Decoder[Attribute[D]]
    def encoder: Encoder[Attribute[D]]
  }

  def apply[D <: Definition: Witness.Aux](v: D#Value): Attribute[D] =
    new Attribute[D](v)

  implicit def cnilAttributeInstances[D <: Definition]: AttributeInstancesAux[D, CNil] =
    new AttributeInstancesAux[D, CNil] {
      override def decoder: Decoder[Attribute[D]] = ???
      override def encoder: Encoder[Attribute[D]] = ???
    }

  implicit def coproductAttributeInstances[D <: Definition, H <: Definition, T <: Coproduct](
      implicit w: Witness.Aux[D],
      wh: Witness.Aux[H],
      d: Decoder[D#Value],
      e: Encoder[D#Value],
      ti: AttributeInstancesAux[D, T]
  ): AttributeInstancesAux[D, CNil] = new AttributeInstancesAux[D, CNil] {
    override def decoder: Decoder[Attribute[D]] = if (wh.value.id == w.value.id) d.map(Attribute[D](_)) else ti.decoder
    override def encoder: Encoder[Attribute[D]] = if (wh.value.id == w.value.id) e.contramap(_.value) else ti.encoder
  }

  implicit def genericAttributeInstances[D <: Definition, Repr <: Coproduct](
      implicit g: Generic.Aux[D, Repr],
      i: AttributeInstancesAux[D, Repr]): AttributeInstances[D] = new AttributeInstances[D] {
    override def decoder: Decoder[Attribute[D]] = i.decoder
  }

}


object DependantTypeDerivation extends App {

  import Attribute._
  import io.circe.syntax._
  // Attribute.gen[List[Attribute[_]]]
  val attributes = List(Name("Jack"), Age(12))
  implicit val de: Decoder[Attribute[Name.type]] = Attribute.coproductAttributeInstances[Name.type, Name.type, CNil].decoder
  implicit val en: Encoder[Attribute[Name.type]] = Attribute.coproductAttributeInstances[Name.type, Name.type, CNil].encoder
  //Attribute.coproductAttributeInstances[Name.type, Age.type, Name.type  :+: CNil]
  //implicit val aa = implicitly[AttributeInstances[Definition]].decoder
  println(Name("Jack").asJson)
}
