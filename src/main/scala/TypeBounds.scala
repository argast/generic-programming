import java.time.LocalDateTime

import shapeless._

case class Something(name: String)

sealed trait Operator[T]

case class EqualTo[T](value: T) extends Operator[T]
case class GreaterThan[T: Ordering](value: T)(implicit ev: T =:= Ordering[T])
    extends Operator[T]
case class YearsSince(value: LocalDateTime) extends Operator[LocalDateTime]

trait OperatorLabel[T] {
  val name: String
}

trait OperatorLabelInstances extends LowLevelOperatorLabel {
  implicit def equalToOperatorLabel[T]: OperatorLabel[EqualTo[T]] =
    new OperatorLabel[EqualTo[T]] {
      override val name: String = "EqualTo"
    }
  implicit def greaterThanOperatorLabel[T: Ordering]
      : OperatorLabel[GreaterThan[T]] =
    new OperatorLabel[GreaterThan[T]] {
      override val name: String = "GreaterThan"
    }
  implicit def yearsSinceOperatorLabel: OperatorLabel[YearsSince] =
    new OperatorLabel[YearsSince] {
      override val name: String = "YearsSince"
    }
}

trait LowLevelOperatorLabel {
  implicit def unknownLabel[T]: OperatorLabel[T] =
    new OperatorLabel[T] {
      override val name: String = "Unknown"
    }
}

/**
  * Filtering of coproduct instances based on output type with additional type class  used to discard not applicable
  * instances (based on implicit context bound)
  */
object TypeBounds extends App with OperatorLabelInstances {

  case class OperatorInfo(name: String)

  trait OperatorDiscovery[PropertyType] {
    def operators(): Seq[OperatorInfo]
  }

  object OperatorDiscovery {

    trait OperatorDiscoveryAux[PropertyType, Repr] {
      def operators(): Seq[OperatorInfo]
    }

    implicit def cnilOperatorDiscovery[PropertyType](
        implicit tpe: Typeable[PropertyType]
    ): OperatorDiscoveryAux[PropertyType, CNil] = () => Seq.empty

    implicit def coproductOperatorDiscovery[PropertyType, H, T <: Coproduct](
        implicit i: H <:< Operator[PropertyType],
        ol: OperatorLabel[H],
        tpe: Typeable[PropertyType],
        td: OperatorDiscoveryAux[PropertyType, T]
    ): OperatorDiscoveryAux[PropertyType, H :+: T] = () => {
      if (ol.name != "Unknown") {
        OperatorInfo(ol.name) +: td.operators
      } else {
        td.operators()
      }
    }

    implicit def genericOperatorDiscovery[PropertyType, Repr <: Coproduct](
        implicit
        gen: Generic.Aux[Operator[PropertyType], Repr],
        sg: Lazy[OperatorDiscoveryAux[PropertyType, Repr]]
    ): OperatorDiscovery[PropertyType] = () => sg.value.operators()
  }

  import OperatorDiscovery._
  println(implicitly[OperatorDiscovery[String]].operators())
  println(implicitly[OperatorDiscovery[LocalDateTime]].operators())
  println(implicitly[OperatorDiscovery[Something]].operators())
}
