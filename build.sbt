name := "generic-programming"

version := "0.1"

scalaVersion := "2.12.9"

libraryDependencies ++= Seq(
  "com.chuusai" %% "shapeless" % "2.3.3",
  "com.propensive" %% "magnolia" % "0.12.0",
  "io.circe" %% "circe-core" % "0.12.3"
)

scalacOptions ++= Seq("-Xlog-implicits", "-P:splain:all:true", "-Ypartial-unification")

addCompilerPlugin("io.tryp" % "splain" % "0.5.0" cross CrossVersion.patch)
